<?php

require_once('ape.php');
require_once('frog.php');

$object = new Animal ("shaun");

echo "Name: " . $object -> name . "<br>";
echo "Legs: " . $object -> legs . "<br>";
echo "Cold blooded: ". $object -> cold_blooded . "<br> <br>";

$object2 = new Frog ("buduk");

echo "Name: " . $object2 -> name . "<br>";
echo "Legs: " . $object2 -> legs . "<br>";
echo "Cold blooded: ". $object2 -> cold_blooded . "<br>";
echo "Yell: " . $object2 -> jump . "<br> <br>";

$object3 = new Ape ("kera sakti");

echo "Name: " . $object3 -> name . "<br>";
echo "Legs: " . $object3 -> legs . "<br>";
echo "Cold blooded: ". $object3 -> cold_blooded . "<br>";
echo "Yell: " . $object3 -> yell . "<br> <br>";